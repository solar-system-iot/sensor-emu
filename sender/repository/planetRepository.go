package repository

import (
	"context"
	"errors"
	"log"
	"sender/config"
	"sender/database"
	"sender/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func FindPlanetInfoCount() (*model.CountModel, error) {
	databaseConfig := config.GetDatabaseConfig()
	collection := database.GetDatabaseClient().Database(databaseConfig.DB_NAME).Collection("count")

	filter := bson.M{}
	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Println("Mongo error", err)
		return nil, err
	}

	var results []model.CountModel
	err = cur.All(context.TODO(), &results)
	if err != nil {
		log.Println("Mongo error", err)
		return nil, err
	}
	return &results[0], nil
}
func FindAllEarthInfo(collectionName string, numberInfo int32) ([]model.PlanetModel, error) {
	databaseConfig := config.GetDatabaseConfig()
	collection := database.GetDatabaseClient().Database(databaseConfig.DB_NAME).Collection(collectionName)

	pipeline := bson.A{
		bson.M{
			"$sort": bson.M{
				"terrestrial_date": 1,
			},
		},
		bson.M{
			"$skip": numberInfo,
		},
		bson.M{
			"$limit": 1,
		},
	}

	cur, err := collection.Aggregate(context.TODO(), pipeline)

	if err != nil {
		log.Println("Mongo error", err)
		return nil, err
	}
	var results []model.PlanetModel
	err = cur.All(context.TODO(), &results)
	if err != nil {
		log.Println("Mongo error", err)
		return nil, err
	}
	return results, nil
}

func IncrementCount(planetName string) (*mongo.UpdateResult, error) {
	countResult, err := FindPlanetInfoCount()
	if err != nil {
		log.Println("Mongo error", err)
	}

	var updateCondition *bson.D
	switch planetName {
	case "earth":
		{
			updateCondition = &bson.D{{"$set", bson.M{"earth_no": countResult.EarthNo + 1}}}
			break
		}
	case "mars":
		{
			updateCondition = &bson.D{{"$set", bson.M{"mars_no": countResult.MarsNo + 1}}}
			break
		}
	case "venus":
		{
			updateCondition = &bson.D{{"$set", bson.M{"venus_no": countResult.VenusNo + 1}}}
			break
		}
	case "kepler":
		{
			updateCondition = &bson.D{{"$set", bson.M{"kepler_no": countResult.KeplerNo + 1}}}
			break
		}
	default:
		{
			return nil, errors.New("planet name is not correctly")
		}
	}

	databaseConfig := config.GetDatabaseConfig()
	collection := database.GetDatabaseClient().Database(databaseConfig.DB_NAME).Collection("count")

	updateResult, err := collection.UpdateByID(context.TODO(), countResult.ID, updateCondition)
	if err != nil {
		log.Println("Mongo error", err)
	}

	return updateResult, nil
}

func AddPlanetInfo(data model.PlanetModel) (*mongo.InsertOneResult, error) {
	databaseConfig := config.GetDatabaseConfig()
	collection := database.GetDatabaseClient().Database(databaseConfig.BACKEND_DB).Collection("planetinfos")

	return collection.InsertOne(context.TODO(), data)
}
