package main

import (
	"encoding/json"
	"log"
	"os"
	"sender/config"
	"sender/database"
	"sender/mq"
	"sender/repository"
	"time"

	"github.com/robfig/cron/v3"
)

func emulate(currentQueue string, routingKey string) {
	for i := 0; i < 50; i++ {
		// log.Println("[Sender job] started")
		countResult, err := repository.FindPlanetInfoCount()
		if err != nil {
			log.Println(err)
		}
		var countValue int32
		switch currentQueue {
		case "earth":
			{
				countValue = countResult.EarthNo
				break
			}
		case "mars":
			{
				countValue = countResult.MarsNo
				break
			}
		case "venus":
			{
				countValue = countResult.VenusNo
				break
			}
		case "kepler":
			{
				countValue = countResult.KeplerNo
				break
			}
		}
		results, err := repository.FindAllEarthInfo(currentQueue, countValue)
		if err != nil {
			log.Println(err)
		}

		channel := mq.CreateChannel(currentQueue, routingKey)

		myJsonString, err := json.Marshal(results[0])
		if err != nil {
			log.Println(err, results)
		} else {
			err := mq.Publish(myJsonString, channel)
			if err != nil {
				log.Println(err)
			}
			_, err = repository.IncrementCount(currentQueue)
			if err != nil {
				log.Println(err)
			}
			// log.Println("[Job 1] Send data")
		}
		channel.Close()
		time.Sleep(1000 * time.Millisecond)
	}
}

func main() {
	config.Init()
	database.Init()
	defer database.Disconnect()
	mq.CreateConnection()

	var currentQueue string
	if len(os.Args) < 2 {
		log.Printf("Usage: %s [info] [warning] [error]", os.Args[0])
		os.Exit(0)
	} else if len(os.Args) == 2 {
		currentQueue = os.Args[1]
	}

	rabbitMQ := config.GetRabbitMQConfig()

	routingKey := currentQueue + rabbitMQ.RoutingKeySuffix

	c := cron.New()
	// c.AddFunc("*/1 * * * *", func() {
	// 	myJsonString := `{"message":` + currentQueue + `}`
	// 	mq.Publish([]byte(myJsonString))
	// 	log.Println("[Job 1] Send data")
	// })
	// emulate(currentQueue, routingKey)

	c.AddFunc("*/1 * * * *", func() {
		countResult, err := repository.FindPlanetInfoCount()
		if err != nil {
			log.Println(err)
		}
		var countValue int32
		switch currentQueue {
		case "earth":
			{
				countValue = countResult.EarthNo
				break
			}
		case "mars":
			{
				countValue = countResult.MarsNo
				break
			}
		case "venus":
			{
				countValue = countResult.VenusNo
				break
			}
		case "kepler":
			{
				countValue = countResult.KeplerNo
				break
			}
		}
		results, err := repository.FindAllEarthInfo(currentQueue, countValue)
		if err != nil {
			log.Println(err)
		}

		channel := mq.CreateChannel(currentQueue, routingKey)

		myJsonString, err := json.Marshal(results[0])
		if err != nil {
			log.Println(err, results)
		} else {
			err := mq.Publish(myJsonString, channel)
			if err != nil {
				log.Println(err)
			}
			_, err = repository.IncrementCount(currentQueue)
			if err != nil {
				log.Println(err)
			}
			log.Println("[Job 1] Send data")
		}
		channel.Close()
	})

	log.Println("Start the new job")
	c.Start()

	forever := make(chan bool)

	defer log.Println("Stop the new job")
	defer c.Stop()

	<-forever
}
