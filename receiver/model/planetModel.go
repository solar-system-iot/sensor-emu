package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PlanetModel struct {
	ID              primitive.ObjectID `bson:"_id" json:"id"`
	PlanetName      string             `bson:"planetName" json:"planetName"`
	TerrestrialDate time.Time          `bson:"terrestrial_date" json:"terrestrial_date"`
	Sol             int32              `bson:"sol" json:"sol"`
	Ls              int32              `bson:"ls" json:"ls"`
	Month           int32              `bson:"month" json:"month"`
	MinTemp         float64            `bson:"min_temp" json:"min_temp"`
	MaxTemp         float64            `bson:"max_temp" json:"max_temp"`
	Pressure        float64            `bson:"pressure" json:"pressure"`
	WindSpeed       float64            `bson:"wind_speed" json:"wind_speed"`
	AtmoOpacity     string             `bson:"atmo_opacity" json:"atmo_opacity"`
	O_a             float64            `bson:"O_a" json:"O_a"`
	N_a             float64            `bson:"N_a" json:"N_a"`
	Ar_a            float64            `bson:"Ar_a" json:"Ar_a"`
	CO2_            float64            `bson:"CO2_a" json:"CO2_a"`
	Other_a         float64            `bson:"other_a" json:"other_a"`
	O_g             float64            `bson:"O_g" json:"O_g"`
	Si_g            float64            `bson:"Si_g" json:"Si_g"`
	Al_g            float64            `bson:"Al_g" json:"Al_g"`
	Fe_g            float64            `bson:"Fe_g" json:"Fe_g"`
	Ca_g            float64            `bson:"Ca_g" json:"Ca_g"`
	Na_g            float64            `bson:"Na_g" json:"Na_g"`
	K_g             float64            `bson:"K_g" json:"K_g"`
	Mg_g            float64            `bson:"Mg_g" json:"Mg_g"`
	Cl_g            float64            `bson:"Cl_g" json:"Cl_g"`
	Other_g         float64            `bson:"other_g" json:"other_g"`
}

// time.Now().UTC().Format(time.RFC3339)
