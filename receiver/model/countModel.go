package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type CountModel struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	EarthNo  int32              `bson:"earth_no" json:"earth_no"`
	MarsNo   int32              `bson:"mars_no" json:"mars_no"`
	VenusNo  int32              `bson:"venus_no" json:"venus_no"`
	KeplerNo int32              `bson:"kepler_no" json:"kepler_no"`
}
